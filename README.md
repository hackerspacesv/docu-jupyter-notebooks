# Archivos de la Charla Jupyter Notebooks

Este repositorio contiene los archivos de la charla **"Documentando Procesos de Análisis de Datos con Jupyter Notebooks: Encuesta de Ingresos de Profesionales de la Computación 2018"**. Estos incluyen:

 - Archivos crudos con los resultados de la encuesta de ingresos
 - Notebook de Jupyter utilizado en la charla
 - Presentación para LibreOffice

Si no te fue posible asistir a la charla, puedes observarla en línea en nuestro canal de Youtube:

-   Parte 1: [https://youtu.be/gLxiYXMe4EQ](https://youtu.be/gLxiYXMe4EQ)  
    
-   Parte 2: [https://youtu.be/doG28bIMlg8](https://youtu.be/doG28bIMlg8)

## Licencia de Uso
Copyright 2018 - Mario Gómez @ Hackerspace San Salvador.
 - Los archivos de datos de este repositorio se distribuyen bajo la [Open Database License (ODbL)](https://opendatacommons.org/licenses/odbl/summary/).
 - Las presentaciones y documentación se distribuyen bajo la licencia [Creative Commons Attribution Share-Alike 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
 - El código fuente de Python incrustado en la documentación se distribuyen bajo la licencia [GNU GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Excepción de Responsabilidades sobre los Datos y Condiciones adicionales de uso:
Al utilizar estos datos en otras publicaciones/investigaciones tomar en consideración qué:

1.  **Estos datos no son representativos de todos los profesionales en Informática en El Salvador:** Para hacer una encuesta realmente representativa necesitaríamos conocer con certeza el universo de población de profesionales en informática en el país. Esto es una tarea bastante difícil considerando que 1ro. No todos los profesionales graduados se desempeñan en el área y 2do. No todos los profesionales del área completaron su carrera. Recordemos que la profesión no está colegiada y por tanto es muy difícil conocer a ciencia cierta cuantos profesionales se desempeñan en el área.
2.  **Existe un sesgo atribuible al medio por el cual se realiza la entrevista que es imposible de corregir o estimar**: Como la encuesta fue realizada en línea se asume que sólo aquellos que tuvieron acceso a Internet, se enteraron o les hicieron llegar la encuesta y tuvieron la oportunidad de llenarla fueron capaces de expresar su opinión. Hay que recordar que estamos en un país con limitada penetración de Internet y es imposible determinar con certeza si los que respondieron la encuesta son realmente una población representativa de todos los informáticos del país.
3.  **Los resultados aquí mostrados podrían sufrir del efecto denominado "sesgo del superviviente" es decir que solo muestran la realidad de los que han logrado obtener un empleo:** Esto significa, que si usted ve que X tecnología está asociada a Y cantidad de ingresos esto no necesariamente significa que si usted se especializa en X tecnología entonces va a tener Y cantidad de ingresos. Ese tipo de conclusiones de correlación solo es posible determinarlos si también tenemos en consideración a aquellos que actualmente no tienen un empleo o que no han entrado a la fuerza laboral, que como expliqué en la limitante #1 es muy difícil de determinar.
4.  **La muestra es muy pequeña y por ende los errores en la estimación pueden ser muy grandes**: Esto significa que a donde usted vea un número X el valor real puede estar muy alejado de este valor por encima de este o por debajo del mismo.

Por lo anterior: los datos incluidos en este repositorio solo podrán ser utilizados, citados o incluidos en otras publicaciones bajo la estricta condición de citar las limitantes antes mencionadas. El no cumplir con lo anterior revoca automáticamente su derecho de utilización.